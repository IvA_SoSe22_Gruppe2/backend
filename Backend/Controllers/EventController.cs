﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;
using Backend.Models;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        public EventController(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
        }

        //Get a list of all events
        [HttpGet("getAllEvents")]
        public JsonResult GetAllNews()
        {
            string query = @"
                            select *
                            from
                            dbo.eventData
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        //Get the events with the user id which matches with the inserted user id
        [HttpGet("getAllEventsByUserID/{id}")]
        public JsonResult GetAllUsers(int id)
        {
            string query = @"
                            select *
                            from dbo.eventData
                            where dbo.eventData.userId = @userId
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@userId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        //Get the event with the id which matches with the inserted id
        [HttpGet("{id}")]
        public JsonResult Get(int id)
        {
            string query = @"
                            select * 
                            from dbo.eventData
                            where dbo.eventData.eventId = @eventId
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@eventId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        //Post a new event
        [HttpPost]
        public JsonResult Post(Event even, int activityId, int userId)
        {
            string query = @"
                            insert into dbo.eventData
                            (activityId, userId, startDate, endDate, duration, distanceInKm, avgSpeed, calories)
                            values (@activityId, @userId, @startDate, @endDate, @duration, @distanceInKm, @avgSpeed, @calories)  
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@activityId", activityId);
                    myCommand.Parameters.AddWithValue("@userId", userId);
                    myCommand.Parameters.AddWithValue("@startDate", even.startDate);
                    myCommand.Parameters.AddWithValue("@endDate", even.endDate);
                    myCommand.Parameters.AddWithValue("@duration", even.duration);
                    myCommand.Parameters.AddWithValue("@distanceInKm", even.distanceInKm);
                    myCommand.Parameters.AddWithValue("@avgSpeed", even.avgSpeed);
                    myCommand.Parameters.AddWithValue("@calories", even.calories);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Event added successfully");
        }

        //Delete a specific event which id matches with the inserted id
        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            string query = @"
                            delete from dbo.eventData
                            where eventId = @eventId
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@eventId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Event deleted successfully");
        }

        //Delete all events
        [HttpDelete("deleteAllEvents")]
        public JsonResult DeleteAllEvents()
        {
            string query = @"
                            truncate table dbo.eventData
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("All events deleted successfully");
        }
    }
}
