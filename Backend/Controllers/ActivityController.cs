﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;
using Backend.Models;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActivityController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        public ActivityController(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
        }

        //Get a list of all activities
        [HttpGet("GetAllActivities")]
        public JsonResult GetAllActivities()
        {
            string query = @"
                            select *
                            from
                            dbo.activityData
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        //Get the activity with the id which matches with the inserted id
        [HttpGet("{id}")]
        public JsonResult Get(int id)
        {
            string query = @"
                            select * 
                            from dbo.activityData
                            where dbo.activityData.activityId = @activityId
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@activityId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        //Post a new activity
        [HttpPost]
        public JsonResult Post(Activity activity)
        {
            string query = @"
                            insert into dbo.activityData
                            (activityName)
                            values (@activityName)              
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@activityName", activity.activityName);                                
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Activity added successfully");
        }

        //Delete a specific activity which id matches with the inserted id
        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            string query = @"
                            delete from dbo.activityData
                            where activityId = @activityId
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@activityId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Activity deleted successfully");
        }

        //Delete all activities
        [HttpDelete("deleteAllActivities")]
        public JsonResult DeleteAllEvents()
        {
            string query = @"
                            truncate table dbo.activityData
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("All activities deleted successfully");
        }
    }
}
