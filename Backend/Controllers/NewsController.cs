﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;
using Backend.Models;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        public NewsController(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
        }

        //Get a list of all news
        [HttpGet("getAllNews")]
        public JsonResult GetAllNews()
        {
            string query = @"
                            select newsId, newsContent, newsDate, newsPicture, newsHeader
                            from
                            dbo.newsData
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        //Get the news with the id which matches with the inserted id
        [HttpGet("{id}")]
        public JsonResult Get(int id)
        {
            string query = @"
                            select * 
                            from dbo.newsData
                            where dbo.newsData.newsId = @newsId
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@newsId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        //Post a new news
        [HttpPost]
        public JsonResult Post(News news)
        {
            string query = @"
                            insert into dbo.newsData
                            (newsContent, newsDate, newsPicture, newsHeader)
                            values (@newsContent, @newsDate, @newsPicture, @newsHeader)       
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@newsContent", news.newsContent);
                    myCommand.Parameters.AddWithValue("@newsDate", news.newsDate);
                    myCommand.Parameters.AddWithValue("@newsPicture", news.newsPicture);
                    myCommand.Parameters.AddWithValue("@newsHeader", news.newsHeader);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("News added successfully");
        }

        //Patch an existing news which id matches with the inserted id
        [HttpPatch("{id}")]
        public JsonResult PatchApp(int id, News news)
        {
            string query = @"
                            update dbo.newsData                            
                            set newsContent = @newsContent, newsDate = @newsDate, newsPicture = @newsPicture, newsHeader = @newsHeader
                            where newsId = @newsId
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@newsId", id);
                    myCommand.Parameters.AddWithValue("@newsContent", news.newsContent);
                    myCommand.Parameters.AddWithValue("@newsDate", news.newsDate);
                    myCommand.Parameters.AddWithValue("@newsPicture", news.newsPicture);
                    myCommand.Parameters.AddWithValue("@newsHeader", news.newsHeader);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("News data updated");
        }

        //Delete a specific news which id matches with the inserted id
        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            string query = @"
                            delete from dbo.newsData
                            where newsId = @newsId
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@newsId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("News deleted successfully");
        }

        //Delete all news
        [HttpDelete("deleteAllNews")]
        public JsonResult DeleteAllEvents()
        {
            string query = @"
                            truncate table dbo.newsData
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("All news deleted successfully");
        }
    }
}
