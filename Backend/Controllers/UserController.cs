﻿//Controller includes all endpoints starting with /user/ and which are connected to userData and loginData tables

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;
using Backend.Models;
using System.Net;
using System.Net.Mail;
using System.ComponentModel.DataAnnotations;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        public UserController(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
        }

        //Get a list of all Users with role = "user"
        [HttpGet("getAllUsers")]
        public JsonResult GetAllUsers()
        {
            string query = @"
                            select dbo.userData.userId, lastName, firstName, gender
                            from dbo.userData
                            left join dbo.loginData
                            on dbo.userData.userId = dbo.loginData.userId
                            where dbo.loginData.role = 'user'
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        //Get a list of all Users with role = "Admins"
        [HttpGet("getAllAdmins")]
        public JsonResult GetAllAdmins()
        {
            string query = @"
                            select dbo.userData.userId, lastName, firstName, gender
                            from dbo.userData
                            left join dbo.loginData
                            on dbo.userData.userId = dbo.loginData.userId
                            where dbo.loginData.role = 'admin'
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        //Get a list of all Users with role = "supervisor"
        [HttpGet("getAllSupervisor")]
        public JsonResult GetAllSupervisor()
        {
            string query = @"
                            select dbo.userData.userId, lastName, firstName, gender
                            from dbo.userData
                            left join dbo.loginData
                            on dbo.userData.userId = dbo.loginData.userId
                            where dbo.loginData.role = 'supervisor'
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        //Get a single user by its id
        [HttpGet("{id}")]
        public JsonResult Get(int id)
        {
            string query = @"
                            select dbo.userData.userId, lastName, firstName, gender, height, weight, age, profilePicture, email, hashedEmail, password, role, active
                            from dbo.userData
                            left join dbo.loginData
                            on dbo.userData.userId = dbo.loginData.userId
                            where dbo.userData.userId = @userId
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@userId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        //Validates a user when hes tries to log in with his credentials and checks if user account exist, is activated and if the credentials match
        [HttpGet("loginValidation")]
        public JsonResult loginValidation(string email, string password)
        {
            string query = @"
                            select * 
                            from dbo.loginData 
                            left join dbo.userData
                            on dbo.loginData.userId =  dbo.userData.userId
                            where dbo.loginData.email = @email
                            and dbo.loginData.password = @password
                            and dbo.loginData.active = 1
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@email", email);
                    myCommand.Parameters.AddWithValue("@password", password);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            if (table.Rows.Count == 0)
            {
                string query2 = @"
                            select * 
                            from dbo.loginData 
                            left join dbo.userData
                            on dbo.loginData.userId =  dbo.userData.userId
                            where dbo.loginData.email = @email
                            and dbo.loginData.password = @password                           
                            ";

                DataTable table2 = new DataTable();
                string sqlDataSource2 = _configuration.GetConnectionString("ActivityAppCon");
                SqlDataReader myReader2;
                using (SqlConnection myCon2 = new SqlConnection(sqlDataSource))
                {
                    myCon2.Open();
                    using (SqlCommand myCommand2 = new SqlCommand(query2, myCon2))
                    {
                        myCommand2.Parameters.AddWithValue("@email", email);
                        myCommand2.Parameters.AddWithValue("@password", password);
                        myReader2 = myCommand2.ExecuteReader();
                        table2.Load(myReader2);
                        myReader2.Close();
                        myCon2.Close();
                    }
                }
                if (table2.Rows.Count == 0)
                {
                    JsonResult wrongData = new JsonResult("Wrong email or incorrect password");
                    wrongData.StatusCode = 404;
                    return wrongData;
                }
                else
                {
                    JsonResult wrongData = new JsonResult("User not validated");
                    wrongData.StatusCode = 403;
                    return wrongData;
                }
            }
            else
            {
                return new JsonResult(table);
            }
        }

        //Post a new user to the database and checks if the email has a valid form and if it is already used by another user
        [HttpPost]
        public JsonResult Post(User user)
        {
            if (new EmailAddressAttribute().IsValid(user.email))
            { 
                string query2 = @"
                            select * 
                            from dbo.loginData                         
                            where dbo.loginData.email = @email2                        
                            ";

            DataTable table2 = new DataTable();
            string sqlDataSource2 = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader2;
            using (SqlConnection myCon2 = new SqlConnection(sqlDataSource2))
            {
                myCon2.Open();
                using (SqlCommand myCommand2 = new SqlCommand(query2, myCon2))
                {
                    myCommand2.Parameters.AddWithValue("@email2", user.email);
                    myReader2 = myCommand2.ExecuteReader();
                    table2.Load(myReader2);
                    myReader2.Close();
                    myCon2.Close();
                }
            }

            if (table2.Rows.Count == 0)
            {
                string query = @"
                            insert into dbo.userData
                            (lastName, firstName, gender, height, weight, age, profilePicture)
                            values (@lastName, @firstName, @gender, @height, @weight, @age, @profilePicture)
                                                
                            insert into dbo.loginData
                            (userId, email, password, role, active, hashedEmail)
                            values ((select top(1) userId from dbo.userData order by userId desc), @email, @password, @role, 0, @hashedEmail)                                   
                            ";

                DataTable table = new DataTable();
                string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
                SqlDataReader myReader;
                using (SqlConnection myCon = new SqlConnection(sqlDataSource))
                {
                    myCon.Open();
                    using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    {
                        myCommand.Parameters.AddWithValue("@lastName", user.lastName);
                        myCommand.Parameters.AddWithValue("@firstName", user.firstName);
                        myCommand.Parameters.AddWithValue("@gender", user.gender);
                        myCommand.Parameters.AddWithValue("@height", user.height);
                        myCommand.Parameters.AddWithValue("@weight", user.weight);
                        myCommand.Parameters.AddWithValue("@age", user.age);
                        myCommand.Parameters.AddWithValue("@profilePicture", user.profilePicture);
                        myCommand.Parameters.AddWithValue("@email", user.email);
                        myCommand.Parameters.AddWithValue("@password", user.password);
                        myCommand.Parameters.AddWithValue("@role", user.role);
                        myCommand.Parameters.AddWithValue("@hashedEmail", user.hashedEmail);
                        myReader = myCommand.ExecuteReader();
                        table.Load(myReader);
                        myReader.Close();
                        myCon.Close();
                    }

                    var fromAddress = new MailAddress("johannes.schulte-huermann@alumni.fh-aachen.de", "Email Verification");
                    var toAddress = new MailAddress(user.email, user.lastName);
                    const string fromPassword = "Passwort!23456";
                    const string subject = "Email Verification";
                    string tmp2 = "Activation link: http://localhost:4200/verify/";
                    string body = tmp2 + user.hashedEmail;

                    var smtp = new SmtpClient
                    {
                        Host = "mail.fh-aachen.de",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential("Js3752s", fromPassword)
                    };
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        Body = body
                    })
                    {
                        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                        smtp.Send(message);
                    }
                }            

                return new JsonResult("User Added Successfully");
            }
            else
            {
                JsonResult wrongData = new JsonResult("Email already used");
                wrongData.StatusCode = 404;
                return wrongData;
            }
            }
            else
            {
                JsonResult wrongData = new JsonResult("Email not valid");
                wrongData.StatusCode = 406;
                return wrongData;
            }
        }

        //Activate a user and also checks if user already exists
        [HttpPost("activate")]
        public JsonResult Activate(string email)
        {
            string query2 = @"
                            select * 
                            from dbo.loginData 
                            where email = @email
                            and active = 0
                            ";

            DataTable table2 = new DataTable();
            string sqlDataSource2 = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader2;
            using (SqlConnection myCon2 = new SqlConnection(sqlDataSource2))
            {
                myCon2.Open();
                using (SqlCommand myCommand2 = new SqlCommand(query2, myCon2))
                {
                    myCommand2.Parameters.AddWithValue("@email", email);
                    myReader2 = myCommand2.ExecuteReader();
                    table2.Load(myReader2);
                    myReader2.Close();
                    myCon2.Close();
                }
            }

            if (table2.Rows.Count == 0)
            {
                JsonResult wrongData = new JsonResult("User is already activated or not yet registered");
                wrongData.StatusCode = 406;
                return wrongData;
            }
            else
            {

                string query = @"
                            update dbo.loginData 
                            set active = 1
                            where email = @email            
                            ";

                DataTable table = new DataTable();
                string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
                SqlDataReader myReader;
                using (SqlConnection myCon = new SqlConnection(sqlDataSource))
                {
                    myCon.Open();
                    using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    {
                        myCommand.Parameters.AddWithValue("@email", email);
                        myReader = myCommand.ExecuteReader();
                        table.Load(myReader);
                        myReader.Close();
                        myCon.Close();
                    }
                }

                return new JsonResult("User activated");
            }
        }

        //Deactivate a user by its id
        [HttpPost("{id}/deactivate")]
        public JsonResult Deactivate(int id)
        {
            string query = @"
                            update dbo.loginData 
                            set active = 0
                            where userId = @userId              
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@userId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("User deactivated");
        }

        //Updates user data in frontend
        [HttpPatch("Frontend/{id}")]
        public JsonResult PatchFrontend(int id, User user)
        {
            string query = @"
                            update dbo.userData                            
                            set lastName = @lastName, firstName = @firstName, gender = @gender, height = @height, weight = @weight, age = @age, profilePicture = @profilePicture
                            where userId = @userId

                            update dbo.loginData 
                            set email = @email, password = @password, role = @role, hashedEmail = @hashedEmail
                            where userId = @userId
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@userId", id);
                    myCommand.Parameters.AddWithValue("@lastName", user.lastName);
                    myCommand.Parameters.AddWithValue("@firstName", user.firstName);
                    myCommand.Parameters.AddWithValue("@gender", user.gender);
                    myCommand.Parameters.AddWithValue("@height", user.height);
                    myCommand.Parameters.AddWithValue("@weight", user.weight);
                    myCommand.Parameters.AddWithValue("@age", user.age);
                    myCommand.Parameters.AddWithValue("@profilePicture", user.profilePicture);
                    myCommand.Parameters.AddWithValue("@email", user.email);
                    myCommand.Parameters.AddWithValue("@password", user.password);
                    myCommand.Parameters.AddWithValue("@role", user.role);
                    myCommand.Parameters.AddWithValue("@hashedEmail", user.hashedEmail);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("User data updated");
        }

        //Updates user data in app
        [HttpPatch("App/{id}")]
        public JsonResult PatchApp(int id, UserApp user)
        {
            string query = @"
                            update dbo.userData                            
                            set gender = @gender, height = @height, weight = @weight, age = @age
                            where userId = @userId
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@userId", id);
                    myCommand.Parameters.AddWithValue("@gender", user.gender);
                    myCommand.Parameters.AddWithValue("@height", user.height);
                    myCommand.Parameters.AddWithValue("@weight", user.weight);
                    myCommand.Parameters.AddWithValue("@age", user.age);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Data updated");
        }

        //Deletes an user
        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            string query = @"
                            delete from dbo.eventData
                            where userId = @userId

                            delete from dbo.loginData
                            where userId = @userId

                            delete from dbo.userData
                            where userId = @userId
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@userId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("User deleted successfully");
        }

        //Deletes all users in the database
        [HttpDelete("deleteAllUsers")]
        public JsonResult DeleteAllEvents()
        {
            string query = @"
                            truncate table dbo.userData
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("ActivityAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("All users deleted successfully");
        }

        //Sends an email to the administrator if a user forgot his password
        [HttpPost("forgotPassword")]
        public JsonResult ForgotPassword(string email)
        {

            var fromAddress = new MailAddress("johannes.schulte-huermann@alumni.fh-aachen.de", "Test");
            var toAddress = new MailAddress("johannes.schulte-huermann@alumni.fh-aachen.de");
            const string fromPassword = "Passwort!23456";
            const string subject = "User forgot password";         
            string body = "User with email: " + email + " forgot his password.";

            var smtp = new SmtpClient
            {
                Host = "mail.fh-aachen.de",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("Js3752s", fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                smtp.Send(message);
            }

            return new JsonResult("Email send");
        }
    }
}
