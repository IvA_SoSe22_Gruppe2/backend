﻿namespace Backend.Models
{
    public class User
    {
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string gender { get; set; }
        public int height { get; set; }
        public int weight { get; set; }
        public DateTime age { get; set; }
        public string profilePicture { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string role { get; set; }
        public string hashedEmail { get; set; }
    }

    public class UserApp
    {
        public string gender { get; set; }
        public int height { get; set; }
        public int weight { get; set; }
        public DateTime age { get; set; }
    }
}
