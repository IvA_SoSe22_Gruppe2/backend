﻿namespace Backend.Models
{
    public class Event
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public long duration { get; set; }
        public double distanceInKm { get; set; }
        public double avgSpeed { get; set; }
        public double calories { get; set; }
    }
}
