﻿namespace Backend.Models
{
    public class Activity
    {
        public string activityName { get; set; }
        public int activityCategory { get; set; }
    }
}