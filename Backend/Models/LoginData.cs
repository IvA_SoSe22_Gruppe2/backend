﻿namespace Backend.Models
{
    public class LoginData
    {
        public string email { get; set; }
        public string password { get; set; }
    }
}
