﻿namespace Backend.Models
{
    public class News
    {
        public string newsContent { get; set; }
        public DateTime newsDate { get; set; }
        public string newsPicture { get; set; }
        public string newsHeader { get; set; }
    }
}